# Docker build
docker build -t php7.2_debian_jessie .

# Extensions path
/etc/php/7.2/mods-available

# Search extensions
apt-cache search php
apt-cache search php7.*
apt-cache search php7.2-*

# Extensions
- calendar.ini
- ctype.ini
- curl.ini
- exif.ini
- fileinfo.ini
- ftp.ini
- gettext.ini
- iconv.ini
- json.ini
- opcache.ini
- pdo.ini
- phar.ini
- posix.ini
- readline.ini
- shmop.ini
- sockets.ini
- sysvmsg.ini
- sysvsem.ini
- sysvshm.ini
- tokenizer.ini

# php -m
root@a7e742c88bd3:/srv# php -m
[PHP Modules]
- calendar
- Core
- ctype
- curl
- date
- exif
- fileinfo
- filter
- ftp
- gettext
- hash
- iconv
- json
- libxml
- openssl
- pcntl
- pcre
- PDO
- Phar
- posix
- readline
- Reflection
- session
- shmop
- sockets
- sodium
- SPL
- standard
- sysvmsg
- sysvsem
- sysvshm
- tokenizer
- Zend OPcache
- zlib

[Zend Modules]
- Zend OPcache

# All packages
- php7.2 - server-side, HTML-embedded scripting language (metapackage)
- php7.2-bcmath - Bcmath module for PHP
- php7.2-bz2 - bzip2 module for PHP
- php7.2-cgi - server-side, HTML-embedded scripting language (CGI binary)
- php7.2-cli - command-line interpreter for the PHP scripting language
- php7.2-common - documentation, examples and common module for PHP
- php7.2-curl - CURL module for PHP
- php7.2-dba - DBA module for PHP
- php7.2-dev - Files for PHP7.2 module development
- php7.2-enchant - Enchant module for PHP
- php7.2-fpm - server-side, HTML-embedded scripting language (FPM-CGI binary)
- php7.2-gd - GD module for PHP
- php7.2-gmp - GMP module for PHP
- php7.2-imap - IMAP module for PHP
- php7.2-interbase - Interbase module for PHP
- php7.2-intl - Internationalisation module for PHP
- php7.2-json - JSON module for PHP
- php7.2-ldap - LDAP module for PHP
- php7.2-mbstring - MBSTRING module for PHP
- php7.2-mysql - MySQL module for PHP
- php7.2-odbc - ODBC module for PHP
- php7.2-opcache - Zend OpCache module for PHP
- php7.2-pgsql - PostgreSQL module for PHP
- php7.2-phpdbg - server-side, HTML-embedded scripting language (PHPDBG binary)
- php7.2-pspell - pspell module for PHP
- php7.2-readline - readline module for PHP
- php7.2-recode - recode module for PHP
- php7.2-snmp - SNMP module for PHP
- php7.2-soap - SOAP module for PHP
- php7.2-sqlite3 - SQLite3 module for PHP
- php7.2-sybase - Sybase module for PHP
- php7.2-tidy - tidy module for PHP
- php7.2-xml - DOM, SimpleXML, WDDX, XML, and XSL module for PHP
- php7.2-xmlrpc - XMLRPC-EPI module for PHP
- php7.2-xsl - XSL module for PHP (dummy)
- php7.2-zip - Zip module for PHP
