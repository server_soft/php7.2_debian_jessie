FROM debian:jessie
MAINTAINER Sander Vergeles <sander.vergeles@gmail.com>

# Install utils
RUN apt-get update && apt-get install -y \
	apt-transport-https \
	lsb-release \
	ca-certificates \
    curl \
    libldap2-dev \
    libssl-dev \
    wget

# Add php repository
RUN echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list && \
    wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg

# Install php-fpm && php extensions
RUN apt-get update && apt-get install -y --no-install-recommends \
	php7.2-fpm \
    php7.2-curl \
	&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

COPY ./configs/php.ini /etc/php/7.2/cli/php.ini
COPY ./configs/php.ini /etc/php/7.2/fpm/php.ini

# Configure FPM to run properly on docker
RUN sed -i "/listen = .*/c\listen = [::]:9000" /etc/php/7.2/fpm/pool.d/www.conf \
    && sed -i "/;access.log = .*/c\access.log = /proc/self/fd/2" /etc/php/7.2/fpm/pool.d/www.conf \
    && sed -i "/;clear_env = .*/c\clear_env = no" /etc/php/7.2/fpm/pool.d/www.conf \
    && sed -i "/;catch_workers_output = .*/c\catch_workers_output = yes" /etc/php/7.2/fpm/pool.d/www.conf \
    && sed -i "/user = .*/c\user = www" /etc/php/7.2/fpm/pool.d/www.conf \
    && sed -i "/group = .*/c\group = www" /etc/php/7.2/fpm/pool.d/www.conf \
    && sed -i "/pid = .*/c\;pid = /run/php/php7.2-fpm.pid" /etc/php/7.2/fpm/php-fpm.conf \
    && sed -i "/;daemonize = .*/c\daemonize = no" /etc/php/7.2/fpm/php-fpm.conf \
    && sed -i "/error_log = .*/c\error_log = /proc/self/fd/2" /etc/php/7.2/fpm/php-fpm.conf \
    && groupadd -g 1000 www \
    && useradd -u 1000 -r -g www www

WORKDIR /srv/
EXPOSE 9000

# The following runs FPM and removes all its extraneous log output on top of what your app outputs to stdout
CMD ["/usr/sbin/php-fpm7.2"]
